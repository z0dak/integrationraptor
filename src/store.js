import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        isUpdate: false,
        currentFeed: null,
        wizzardValues:
        {
            customerId: null,
            customerName: "",
            feedName: "",
            url: "",
            description: "",
            schema: [
                {
                    ProductId: '',
                    ProductName: '',
                    ProductDescription: '',
                    InStock: null,
                    Price: '',
                    OnSalePrice: '',
                    RetailPrice: '',
                    LastDeleveryDate: '',
                    BrandId: '',
                    BrandName: '',
                    ProductUrl: '',
                    ImageUrl: ''
                }
            ]
        }
    },
    getters: {
        getWizzardObject: state => {
            return state.wizzardValues
        },
        getIsUpdate: state => {
            return state.isUpdate
        },
        getCurrentFeed: state => {
            return state.currentFeed
        }
    },
    mutations: {
        setCurrentFeed: (state, data) => {
            state.currentFeed = data
        },
        setIsUpdate: (state, data) => {
            state.isUpdate = data
        },
        addWizzardSchema: (state, data) => {
            
            state.wizzardValues.schema.push(data)
        },
        updateWizzard: (state, data) => {
            state.wizzardValues = {...state.wizzardValues,  ...data}
           
        },
        updateWizzardSchema: (state, data) => {
            state.wizzardValues.schema = data
        },
    },
    actions: {}
});

