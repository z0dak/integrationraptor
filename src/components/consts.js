export const tabelHeaders = [
  {
    text: 'Name',
    align: 'start',
    sortable: true,
    value: 'name',
  },
  { text: 'Customer', value: 'customer', sortable: true, },
  { text: 'Status', value: 'status', sortable: true, },
  { text: 'Actions', value: 'actions', sortable: false },
];
export const tableItems = [
  {
    name: 'Pricerunner feed',
    customer: 'Tesco',
    status: 'Imported',
  },
  {
    name: 'Danish ProductFeed',
    customer: 'Customer A',
    status: 'Not Started',
  },
  {
    name: 'German ProductFeed',
    customer: 'Customer B',
    status: "Importing",
  },
  {
    name: 'Trustpilot',
    customer: 'Huge Company',
    status: 'Imported',
  },
  {
    name: 'Pricerunner2',
    customer: 'Huge Company',
    status: 'Imported',
  },
  {
    name: 'Products',
    customer: 'Huge Company2',
    status: 'Imported',
  },

]
export const customerItems = [
  {
    // id: 1,
    name: "Tesco",
  },
  {
    // id: 3,
    name: "Customer A",
  },
  {
    // id: 4,
    name: "Customer B",
  },
  {
    // id: 5,
    name: "Huge Company",
  },
  {
    // id: 6,
    name: "Huge Company2",
  }
]
export const feedsFromDB = [
  {
    customerId: 1,
    customerName: 'Tesco',
    feedName: "Pricerunner feed",
    url: "www.tesco.com",
    description: "tescoFTW",
    schema: [
      {
      ProductId: 'Product 1',
      ProductName: 'Amazon Prime',
      ProductDescription: 'Some desc',
      InStock: true,
      Price: '$100',
      OnSalePrice: '$50',
      RetailPrice: '$300',
      LastDeleveryDate: 'yesterday',
      BrandId: '12-3344',
      BrandName: 'AmazonP',
      ProductUrl: 'wwww.amazon.com',
      ImageUrl: ''
    }
    ]
  },
  {
    customerId: 2,
    customerName: "Customer A",
    feedName: "Danish ProductFeed",
    url: "www.danish.com",
    description: "danishFTW",
    schema: [
      {
      ProductId: '',
      ProductName: '',
      ProductDescription: '',
      InStock: null,
      Price: '',
      OnSalePrice: '',
      RetailPrice: '',
      LastDeleveryDate: '',
      BrandId: '',
      BrandName: '',
      ProductUrl: '',
      ImageUrl: ''
    }
    ]
  },
  {
    customerId: 3,
    customerName: "Huge Company",
    feedName: "German ProductFeed",
    url: "https://hugeCompany.com",
    description: "huuuge",
    schema: [
      {
      ProductId: '',
      ProductName: '',
      ProductDescription: '',
      InStock: null,
      Price: '',
      OnSalePrice: '',
      RetailPrice: '',
      LastDeleveryDate: '',
      BrandId: '',
      BrandName: '',
      ProductUrl: '',
      ImageUrl: ''
    }
    ]
  }
]


export const schemaItems = [
  {
    text: 'ProductId',
    value: 'ProductId'
  },
  {
    text: 'ProductName',
    value: 'ProductName'
  },
  {
    text: 'ProductCustomer',
    value: 'ProductCustomer'
  },
  {
    text: 'Product',
    value: 'Product'
  },
  {
    text: 'ProductUrl',
    value: 'ProductUrl'
  },
  {
    text: 'Price',
    value: 'Price'
  },
  {
    text: 'OnSalePrice',
    value: 'OnSalePrice'
  },
  {
    text: 'RetailPrice',
    value: 'RetailPrice'
  },
  {
    text: 'LastDeleveryDate',
    value: 'LastDeleveryDate'
  },
  {
    text: 'BrandId',
    value: 'BrandId'
  },
  {
    text: 'BrandName',
    value: 'BrandName'
  },
  {
    text: 'ImageUrl',
    value: 'ImageUrl'
  },

]