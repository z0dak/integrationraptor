
const routes = [
    {
      path: '/',
      component: () => import('components/IntegrationTable.vue'),
      children: [
        { path: '', component: () => import('components/IntegrationWizzard/firstPage.vue') }
      ]
    }
  ]
  const app = new Vue({
    router
  }).$mount('#app')
  // Always leave this as last one
  if (process.env.MODE !== 'ssr') {
    routes.push({
      path: '*',
      component: () => import('pages/Error404.vue')
    })
  }
  
  export default routes
  